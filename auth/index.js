const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const { User } = require('../models');
const { authConfig } = require('../config');
const { AppError, errorConfig } = require('../errors');

const localOpt = {
    usernameField: 'email',
    passwordField: 'password'
};

const jwtOpt = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: authConfig.jwtSecret
};

// Passport local strategy to authenticate users using email and password
// To be used on the login route
passport.use(new LocalStrategy(localOpt,
    async (email, password, done) => {
        const user = await User.getByEmail(email, true);
        if (!user) { return done(null, false); }

        const validPassword = await user.validPassword(password);

        if (!validPassword) {
            return done(null, false);
        }

        return done(null, user);
    }
));

// JWT Passport strategy to authenticate requests carrying a token
passport.use(new JwtStrategy(jwtOpt,
    async (jwt_payload, done) => {
        const user = await User.get(jwt_payload.id, true);

        return done(null, user);
    }
));

// Middleware that utilizes JWT strategy to guard routes
module.exports.authenticate = (req, res, next) => {
    passport.authenticate('jwt', { session: false },
        (err, user, info) => {
            if (err || !user) {
                return next(new AppError(errorConfig.NotAuthorized));
            }
            req.user = user;
            next();
        })(req, res, next);
}
