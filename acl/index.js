let acl = require('acl');
const { aclConfig } = require('../config');
const { AppError, errorConfig } = require('../errors');

// Configuring acl
acl = new acl(new acl.memoryBackend());

for (let role of aclConfig) {
    for (let entry of role.permissions) {
        acl.allow(role.roleName, entry.resource, entry.actions);
    }
}


// Middleware function to protect routes according to permissions and roles of the current user
module.exports.authorize = (resource, permissions) => {
    return async (req, res, next) => {
        const role_names = req.user.roles.map(role => role.name);

        const allowed = await acl.areAnyRolesAllowed(role_names, resource, permissions);

        if (!allowed) {
            return next(new AppError(errorConfig.NotAuthorized));
        }

        next();
    }
}
