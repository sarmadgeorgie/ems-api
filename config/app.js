const env = process.env;

// Genera app configuration
module.exports.appConfig = {
    appName:        env.APP_NAME        || 'EMS',
    port:           env.PORT            || 3000
};
