// Default set of RBAC configuration
module.exports.aclConfig = [
    {
        roleName: 'admin',
        permissions: [
            { resource: 'departments', actions: ['read', 'insert'] },
            { resource: 'users', actions: ['read', 'insert'] },
            { resource: 'roles', actions: ['read'] }
        ]
    },
    {
        roleName: 'superadmin',
        permissions: [
            { resource: 'departments', actions: '*' },
            { resource: 'users', actions: '*' },
            { resource: 'roles', actions: ['*'] }
        ]
    }
];
