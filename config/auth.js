const env = process.env;

// User authentication configuration
module.exports.authConfig = {
    saltRounds:     +env.SALT_ROUNDS    || 10,
    jwtSecret:      env.JWT_SECRET      || 'Developement Password',
    jwtExpiresIn:   env.JWT_EXPIRES_IN  || '7d'
};
