
// CORS default config
module.exports.corsConfig = {
    origin: [
        'http://localhost:4200'
    ],
    optionsSuccessStatus: 200,
    maxAge: 600,
}