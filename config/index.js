/**
 * Store default app configuration in this directory
 * Some values could be overriden in the .env file
 */

module.exports = {
    ...require('./app'),
    ...require('./knex'),
    ...require('./cors'),
    ...require('./auth'),
    ...require('./acl'),
    ...require('./query'),
};
