// Default configurations for query values of the API endpoints

// Common query defaults
const commonQueryDefaults = {
    query: {
        limit: 20,
        page: 1,
        sortBy: 'id',
        sortOrder: 'asc'
    },
    validation: {
        limitMin: 1,
        limitMax: 50
    }
};

// Department Query default values
module.exports.departmentsQueryDefaults = {
    ...commonQueryDefaults,
    query: {
        ...commonQueryDefaults.query,
        userCount: 0,
    }
};

// Single department query default values
module.exports.singleDepartmentQueryDefaults = {
    query: {
        userCount: 1
    }
}

// Users query default values
module.exports.usersQueryDefaults = commonQueryDefaults;

// Single user query defaults
module.exports.singleUserQueryDefaults = {
    query: {
        department: 0,
        roles: 0
    }
}

// Roles query default values
module.exports.rolesQueryDefaults = commonQueryDefaults;
