
const env = process.env;

// Database configuration
const dbConfig = module.exports.dbConfig = {
    dbClient:       env.DB_CLIENT       || 'mysql',
    dbHost:         env.DB_HOST         || 'localhost',
    dbPort:         env.DB_PORT         || 3306,
    dbName:         env.DB_NAME         || 'ems',
    dbUser:         env.DB_USER         || 'ems',
    dbPass:         env.DB_PASS         || 'ems_db_pass',
    dbPoolMin:      +env.DB_POOL_MIN    || 2,
    dbPoolMax:      +env.DB_POOL_MAX    || 10
};

// Knex configuration
module.exports.knexConfig = {
    client: dbConfig.dbClient,
    connection: {
        host:       dbConfig.dbHost,
        port:       dbConfig.dbPort,
        database:   dbConfig.dbName,
        user:       dbConfig.dbUser,
        password:   dbConfig.dbPass
    },
    pool: {
        min: dbConfig.dbPoolMin,
        max: dbConfig.dbPoolMax
    },
    migrations: {
        tableName: 'knex_migrations'
    }
};
