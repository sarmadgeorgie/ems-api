require('dotenv');
const { authConfig } = require('../config');
const bcrypt = require('bcrypt');
const faker = require('faker');


exports.seed = async function (knex, Promise) {
    const password = await bcrypt.hash('password', authConfig.saltRounds);

    const randomUsersCount = 150;
    const randomUsers = [];

    for (let i = 0; i < randomUsersCount; i++) {
        randomUsers.push({
            first_name: faker.name.firstName(),
            last_name: faker.name.lastName(),
            email: faker.internet.email(),
            department_id: faker.random.number({ min: 1, max: 8 }),
            password: await bcrypt.hash(faker.random.uuid(), authConfig.saltRounds)
        });
    }

    // Insert users
    await knex('users').insert([
        { id: 1, first_name: 'Sarmad', last_name: 'Georgie', department_id: 4, email: 'sarmadmjg@gmail.com', password },
        { id: 2, first_name: 'John', last_name: 'Doe', department_id: 1, email: 'j.doe@gmail.com', password },
        { id: 3, first_name: 'Jane', last_name: 'Doe', department_id: 1, email: 'j.doe32@gmail.com', password },
        ...randomUsers
    ]);

    // Assign roles
    await knex('users_roles').insert([
        { user_id: 2, role_id: 1 },
        { user_id: 3, role_id: 1 },

        { user_id: 1, role_id: 2 }
    ]);
};
