
exports.seed = async function (knex, Promise) {
    // Create roles
    await knex('roles').insert([
        { id: 1, name: 'admin', common_name: 'Adminstrator' },
        { id: 2, name: 'superadmin', common_name: 'Super Admin' }
    ]);
};
