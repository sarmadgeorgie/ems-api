
exports.seed = async function (knex, Promise) {
    // Deletes ALL existing entries
    await knex('users_roles').del();
    await knex('departments').del();
    await knex('users').del();
    await knex('roles').del();

    await knex('departments').insert([
        { id: 1, name: 'Human Resources' },
        { id: 2, name: 'Finance' },
        { id: 3, name: 'Advertisement' },
        { id: 4, name: 'Research and development' },
        { id: 5, name: 'Customer Relations' },
        { id: 6, name: 'Sales' },
        { id: 7, name: 'Supply chain' },
        { id: 8, name: 'IT' },
    ]);
};
