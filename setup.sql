CREATE DATABASE ems;
CREATE USER 'ems'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ems_db_pass';
GRANT ALL PRIVILEGES ON ems.* TO 'ems'@'localhost';
