/**
 * Department routes
 */

const Router = require('express').Router;
const queryIntParser = require('express-query-int');
const { Department } = require('../models');
const { validateQuery, departmentsQuerySchema, singleDepartmentQuerySchema, setQueryDefaults } = require('../utils/validators');
const { departmentsQueryDefaults, singleDepartmentQueryDefaults } = require('../config');
const { AppError, errorConfig } = require('../errors');
const { authenticate } = require('../auth');
const { authorize } = require('../acl');


const router = Router();

router.use(authenticate);

// Search departments
router.get('/',
    authorize('departments', 'read'),
    queryIntParser(),
    validateQuery(departmentsQuerySchema),
    setQueryDefaults(departmentsQueryDefaults.query),
    async (req, res) => {
        const result = await Department.search(req.query);

        res.json(result);
    }
);


// Create department
router.post('/',
    authorize('departments', 'insert'),
    async (req, res) => {
        const department = await Department.insert(req.body);

        res.status(201);
        res.json(department);
    }
);


// Get a department by id
router.get('/:id',
    authorize('departments', 'read'),
    queryIntParser(),
    validateQuery(singleDepartmentQuerySchema),
    setQueryDefaults(singleDepartmentQueryDefaults),
    async (req, res) => {
        const department = await Department.get(req.params.id, req.query);

        if (!department) {
            throw new AppError(errorConfig.NotFound);
        }

        res.json(department);
    }
);


// Update a department
router.put('/:id',
    authorize('departments', 'update'),
    async (req, res) => {
        const department = await Department.update(req.params.id, req.body);

        res.status(201);
        res.json(department);
    }
);


// delete a department
router.delete('/:id',
    authorize('departments', 'delete'),
    async (req, res) => {
        const department = await Department.delete(req.params.id);

        res.json(department);
    }
);


module.exports = router;
