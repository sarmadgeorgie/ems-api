/**
 * Users routes
 */

const Router = require('express').Router;
const { Role } = require('../models');
const { AppError, errorConfig } = require('../errors');
const { authenticate } = require('../auth');
const { authorize } = require('../acl');


const router = Router();

router.use(authenticate);

// Get roles
router.get('/',
    authorize('roles', 'read'),
    async (req, res) => {
        const result = await Role.getAll();

        res.json(result);
    }
);

module.exports = router;
