const Router = require('express').Router;
const jwt = require('jsonwebtoken');
const passport = require('passport');
const { AppError, errorConfig } = require('../errors');
const { authConfig } = require('../config');


const router = Router();

// login with email and password
router.use('/login',
    (req, res, next) => {
        passport.authenticate('local', { session: false },
            (err, user, info) => {
                if (!user) { return next(new AppError(errorConfig.AuthenticationError)); }
                if (err) { return next(new AppError(errorConfig.AuthenticationError)); }

                req.login(user, { session: false }, (err) => {
                    if (err) { return next(new AppError(errorConfig.AuthenticationError)); }

                    const token = jwt.sign(user.toJSON(), authConfig.jwtSecret, { expiresIn: '7d' });

                    const decoded = jwt.decode(token);

                    return res.json({ user, token });
                });
        })(req, res);
    }
);


module.exports = router;
