const Router = require('express').Router;


const router = Router();

router.use('/auth', require('./auth'));
router.use('/users', require('./users'));
router.use('/departments', require('./departments'));
router.use('/roles', require('./roles'));


module.exports = router;
