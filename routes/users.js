/**
 * Users routes
 */

const Router = require('express').Router;
const queryIntParser = require('express-query-int');
const { User } = require('../models');
const { validateQuery, usersQuerySchema, setQueryDefaults, singleUserQuerySchema } = require('../utils/validators');
const { usersQueryDefaults, singleUserQueryDefaults } = require('../config');
const { AppError, errorConfig } = require('../errors');
const { authenticate } = require('../auth');
const { authorize } = require('../acl');


const router = Router();

router.use(authenticate);

// Search users
router.get('/',
    authorize('users', 'read'),
    queryIntParser(),
    validateQuery(usersQuerySchema),
    setQueryDefaults(usersQueryDefaults.query),
    async (req, res) => {
        const result = await User.search(req.query);

        res.json(result);
    }
);


// Create user
router.post('/',
    authorize('users', 'insert'),
    async (req, res) => {
        const user = await User.insert(req.body);

        res.status(201);
        res.json(user);
    }
);


// Get a user by id
router.get('/:id',
    authorize('users', 'read'),
    queryIntParser(),
    validateQuery(singleUserQuerySchema),
    setQueryDefaults(singleUserQueryDefaults.query),
    async (req, res) => {
        const user = await User.get(req.params.id, false, req.query);

        if (!user) {
            throw new AppError(errorConfig.NotFound);
        }

        res.json(user);
    }
);


// Update a user
router.put('/:id',
    authorize('users', 'update'),
    async (req, res) => {
        const user = await User.update(req.params.id, req.body);

        res.status(201);
        res.json(user);
    }
);


// delete a user
router.delete('/:id',
    authorize('users', 'delete'),
    async (req, res) => {
        const user = await User.delete(req.params.id);

        res.json(user);
    }
);


module.exports = router;
