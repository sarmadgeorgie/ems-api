
exports.up = async function(knex, Promise) {
    await knex.schema.createTable('departments', table => {
        table.increments('id').primary();
        table.string('name').unique().notNullable();
        table.timestamps();
    });

    await knex.schema.table('users', table => {
        table.integer('department_id').unsigned().notNullable().references('departments.id');
    });
};

exports.down = async function(knex, Promise) {
    await knex.schema.table('users', table => {
        table.dropForeign('department_id');
        table.dropColumn('department_id');
    });

    await knex.schema.dropTable('departments');
};
