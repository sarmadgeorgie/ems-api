
exports.up = async function (knex, Promise) {
    await knex.schema.dropTable('roles_permissions');
    await knex.schema.dropTable('permissions');
};

exports.down = async function (knex, Promise) {
    await knex.schema.createTable('permissions', table => {
        table.increments('id').primary();
        table.string('resource').notNullable();
        table.string('operation').notNullable();
        table.timestamps(true, true);
    });
    await knex.schema.createTable('roles_permissions', table => {
        table.integer('role_id').unsigned().notNullable().references('roles.id');
        table.integer('permission_id').unsigned().notNullable().references('permissions.id');
        table.primary(['role_id', 'permission_id']);
        table.timestamps(true, true);
    });
};
