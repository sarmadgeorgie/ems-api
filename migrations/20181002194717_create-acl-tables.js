
exports.up = async function(knex, Promise) {
    await knex.schema.createTable('roles', table => {
        table.increments('id').primary();
        table.string('name').unique().notNullable();
        table.string('common_name').unique().notNullable();
    });
    await knex.schema.createTable('permissions', table => {
        table.increments('id').primary();
        table.string('resource').notNullable();
        table.string('operation').notNullable();
    });
    await knex.schema.createTable('roles_permissions', table => {
        table.integer('role_id').unsigned().notNullable().references('roles.id');
        table.integer('permission_id').unsigned().notNullable().references('permissions.id');
        table.primary(['role_id', 'permission_id']);
    });
    await knex.schema.createTable('users_roles', table => {
        table.integer('user_id').unsigned().notNullable().references('users.id');
        table.integer('role_id').unsigned().notNullable().references('roles.id');
        table.primary(['user_id', 'role_id']);
    });
};

exports.down = async function(knex, Promise) {
    await knex.schema.dropTable('users_roles');
    await knex.schema.dropTable('roles_permissions');
    await knex.schema.dropTable('permissions');
    await knex.schema.dropTable('roles');
};
