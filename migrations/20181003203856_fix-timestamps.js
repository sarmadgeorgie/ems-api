
exports.up = async function(knex, Promise) {
    await knex.schema.table('departments', async table => {
        await table.dropTimestamps();
    });
    await knex.schema.table('departments', table => {
        table.timestamps(true, true);
    });
    await knex.schema.table('users', async table => {
        await table.dropTimestamps();
    });
    await knex.schema.table('users', table => {
        table.timestamps(true, true);
    });
    await knex.schema.table('roles', table => {
        table.timestamps(true, true);
    });
    await knex.schema.table('permissions', table => {
        table.timestamps(true, true);
    });
};

exports.down = async function(knex, Promise) {
    await knex.schema.table('departments', async table => {
        await table.dropTimestamps();
    });
    await knex.schema.table('departments', table => {
        table.timestamps();
    });
    await knex.schema.table('users', async table => {
        await table.dropTimestamps();
    });
    await knex.schema.table('users', table => {
        table.timestamps();
    });
    await knex.schema.table('roles', table => {
        table.dropTimestamps();
    });
    await knex.schema.table('permissions', table => {
        table.dropTimestamps();
    });
};
