const { errorConfig } = require('./error-config');

module.exports.mysqlErrorConfig = {
    ER_DUP_ENTRY: errorConfig.AlreadyExists
};
