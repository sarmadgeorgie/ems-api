module.exports = {
    ...require('./AppError'),
    ...require('./error-config'),
    ...require('./mysql-errors'),
    ...require('./handlers')
}