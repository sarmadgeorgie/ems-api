require('express-async-errors');
const { AppError } = require('./AppError');
const { mysqlErrorConfig } = require('./mysql-errors');

// Middleware to handle and wrap mysql errors
module.exports.mysqlErrorHandler = mysqlErrorHandler = (err, req, res, next) => {
    const mysqlError = mysqlErrorConfig[err.code];

    if (mysqlError) {
        return next(new AppError(mysqlError));
    }

    return next(err);
}

// Middleware to handle and wrap errors thrown in the app
module.exports.appErrorHandler = appErrorHandler = (err, req, res, next) => {
    if (err instanceof AppError) {
        res.status(err.status);
        return res.json(err);
    }
    next(err);
}

module.exports.applyHandlers = (app) => {
    app.use(mysqlErrorHandler);
    app.use(appErrorHandler);
}
