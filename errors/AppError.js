const errors = require('./error-config');

// Base class to represent and wrap errors thrown by the application
module.exports.AppError = class extends Error {
    constructor(options, data) {
        super();

        this.error = true;

        if (!options) {
            options = errors.ServerError;
        }

        if (typeof options === 'object') {
            Object.assign(this, options);
        }

        this.data = data;
    }
}
