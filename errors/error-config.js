// Common configuration of error to create instances of AppError

module.exports.errorConfig = {
    // Generic errors
    ClientError: {
        type:       'ClientError',
        status:     400,
        message:    'Client error'
    },
    ServerError: {
        type:       'ServerError',
        status:     500,
        message:    'Server error'
    },
    QueryError: {
        type:       'ValidationError',
        status:     400,
        message:    'Invalid query'
    },

    // Authentication errors
    NotAuthorized: {
        type:       'AuthenticationError',
        status:     403,
        message:    'Not authorized'
    },
    WrongCredentials: {
        type:       'AuthenticationError',
        status:     401,
        message:    'Wrong email or password'
    },
    AuthenticationError: {
        type:       'AuthenticationError',
        status:     400,
        message:    'Authentication error'
    },

    // Resource errors
    NotFound: {
        type:       'NotFound',
        status:     404,
        message:    'Not found'
    },
    AlreadyExists: {
        type:       'AlreadyExists',
        status:     400,
        message:    'Already exists'
    },
    ValidationError: {
        type:       'ValidationError',
        status:     400,
        message:    'A required field is missing'
    }
};
