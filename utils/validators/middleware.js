const Validator = require('jsonschema').Validator;
const { AppError, errorConfig } = require('../../errors');

// Middleware to validate the reuquest query against a JSON schema
module.exports.validateQuery = function(schema) {
    return (req, res, next) => {
        const v = new Validator();
        const result = v.validate(req.query, schema);

        if (result && result.errors && result.errors.length > 0) {
            throw new AppError(errorConfig.QueryError, result.errors);
        }

        next();
    }
}

// Middleware to inject some default values into the query object
module.exports.setQueryDefaults = function(defaults) {
    return (req, res, next) => {
        req.query = {
            ...defaults,
            ...req.query
        };

        next();
    }
}
