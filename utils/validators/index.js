module.exports = {
    ...require('./middleware'),
    ...require('./schemas')
};
