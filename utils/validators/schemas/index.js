// This file contains the JSON Schemas for the request queries
// These can be used to validate the request before proceeding

const {
    departmentsQueryDefaults,
    usersQueryDefaults
} = require('../../../config');

// Create common pagination props
function makePaginationProps(config) {
    return {
        'limit': {
            type: 'number',
            minimum: 1,
            maximum: 50
        },
        'page': {
            type: 'number',
            minimum: 1,
            maximum: 50
        }
    };
}

// Create common sort props
function makeSortProps(allowedFields) {
    allowedFields = allowedFields || ['id'];

    return {
        'sortBy': {
            type: 'string',
            enum: allowedFields
        },
        'sortOrder': {
            type: 'string',
            enum: ['asc', 'desc']
        }
    };
}

// Query schema for fetching a list of departments
module.exports.departmentsQuerySchema = {
    type: 'object',
    properties: {
        ...makePaginationProps(departmentsQueryDefaults.validation),
        ...makeSortProps(['id', 'name']),
        'name': {
            type: 'any',
        },
        'userCount': {
            type: 'integer',
            minimum: 0,
            maximum: 1
        }
    }
};

// Query schema for fetching a single department
module.exports.singleDepartmentQuerySchema = {
    type: 'object',
    properties: {
        'userCount': {
            type: 'integer',
            minimum: 0,
            maximum: 1
        }
    }
}

// Query schema for fetching a list of users
module.exports.usersQuerySchema = {
    type: 'object',
    properties: {
        ...makePaginationProps(usersQueryDefaults.validation),
        ...makeSortProps(['id', 'first_name', 'last_name', 'department_id']),
        'name': {
            type: 'any',
        },
        'department_id': {
            type: 'integer',
            minimum: 1
        }
    }
};

// Query schema for fetching a single user
module.exports.singleUserQuerySchema = {
    type: 'object',
    properties: {
        'department': {
            type: 'integer',
            minimum: 0,
            maximum: 1
        },
        'roles': {
            type: 'integer',
            minimum: 0,
            maximum: 1
        }
    }
}
