require('dotenv').config();
require('./auth');
const express = require('express');
const bodyParser = require('body-parser');
const { appConfig, knexConfig, corsConfig } = require('./config');
const { setupDB } = require('./db');
const { applyHandlers } = require('./errors');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors(corsConfig));

app.use('/api/v1', require('./routes'));

applyHandlers(app);

function start() {
    app.listen(appConfig.port, () => {
        console.log(`Serving on port ${appConfig.port}`);
    });
}

setupDB(knexConfig).then(start);
