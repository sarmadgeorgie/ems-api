const { Model } = require('objection');
const bcrypt = require('bcrypt');
const { customModel } = require('./mixins');
const { authConfig } = require('../config');
const uuid = require('uuid/v1');


class User extends customModel(Model) {
    static get tableName() {
        return 'users';
    }

    static get secureFields() {
        return ['password'];
    }

    static get relationMappings() {
        return {
            department: {
                relation: Model.BelongsToOneRelation,
                modelClass: require('./Department').Department,
                join: {
                    from: 'users.department_id',
                    to: 'departments.id'
                }
            },
            roles: {
                relation: Model.ManyToManyRelation,
                modelClass: require('./Role').Role,
                join: {
                    from: 'users.id',
                    through: {
                        from: 'users_roles.user_id',
                        to: 'users_roles.role_id',
                    },
                    to: 'roles.id'
                }
            }
        }
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['email', 'first_name', 'last_name', 'department_id'],

            properties: {
                id: { type: 'integer' },
                email: { type: 'string', minLength: 1 },
                first_name: { type: 'string', minLength: 1 },
                last_name: { type: 'string', minLength: 1 },
                department_id: { type: 'integer', minimum: 1 }
            }
        }
    }

    async $beforeInsert(context) {
        // Insert random password if non provided
        if (!this.password) {
            this.password = uuid();
        }

        // hash password
        this.password = await User.hashPassword(this.password);

        return super.$beforeInsert(context);
    }

    // get a single user
    static async get(id, withRoles, opts) {
        opts = opts || {};
        const query = User.query();

        const eager = [];

        if (opts.department) {
            eager.push('department')
        }

        if (withRoles || opts.roles) {
            eager.push('roles');
        }

        query.eager(`[${eager.toString()}]`);

        return await query.findById(id);
    }

    // get a single user by email
    static async getByEmail(email, withRoles) {
        const query = User.query();

        if (withRoles) {
            query.eager('roles');
        }

        return await query.findOne({ email });
    }

    // Insert a user
    static async insert(data) {
        return await User.query()
            .insertAndFetch(data);
    }

    // Update a user
    static async update(id, data) {
        return await User.query()
            .updateAndFetchById(id, data);
    }

    // Delete a user
    static async delete(id) {
        return await User.query()
            .deleteById(id);
    }

    // Fetch a list of users
    static async search(opts) {
        const query = User.query()
            .limit(opts.limit)
            .offset((opts.page - 1) * opts.limit)
            .orderBy(opts.sortBy, opts.sortOrder);

        if (opts.department_id) {
            query
                .where('department_id', opts.department_id);
        }

        if (opts.name) {
            query
                .where(function() {
                    this.where('first_name', 'like', `%${opts.name}%`)
                        .orWhere('last_name', 'like', `%${opts.name}%`)
                });
        }

        return await query;
    }

    // Helper function to hash passwords
    static async hashPassword(password) {
        return await bcrypt.hash(password, authConfig.saltRounds);
    }

    // Helper function to validate passwords against stored hashes
    async validPassword(password) {
        return await bcrypt.compare(password, this.password);
    }
}

module.exports = { User };
