const { AppError, errorConfig } = require('../errors');
const { omit, pick } = require('lodash');


// A mixin to create a derived class from Objection Model
// This is done with a mixin rather than directly extending
// the class as per the documentation recommendation
module.exports.customModel = function(model) {

    return class extends model {

        // Override converting to json
        $formatJson(json, opt) {
            json = super.$formatJson(json, opt);

            // take only serializable fields
            if (this.constructor.serializable) {
                json = pick(json, this.constructor.serializable);
            }

            // omit secure fields
            if (this.constructor.secureFields) {
                json = omit(json, this.constructor.secureFields);
            }

            return json;
        }

        // modify the instance before inserting into db
        $beforeInsert(context) {
            this.created_at = new Date()
                .toISOString()
                .slice(0, 19)
                .replace('T', ' ');
            this.updated_at = new Date()
                .toISOString()
                .slice(0, 19)
                .replace('T', ' ');

            // prevent inserting ids
            if (this.id) {
                throw new AppError(errorConfig.ClientError, { forbidden_field: 'id' });
            }
            return super.$beforeInsert(context);
        }

        // modify the instance before updating
        $beforeUpdate() {
            this.updated_at = new Date()
                .toISOString()
                .slice(0, 19)
                .replace('T', ' ');
            delete this.created_at;
        }

        // override validating against the JSON schema
        $validate(json, opt) {
            try {
                return super.$validate(json, opt);
            } catch (err) {
                if (err instanceof ValidationError) {
                    throw new AppError({
                        status:     400,
                        name:       err.name,
                        type:       err.type,
                        message:    err.message,
                    }, err.data);
                } else {
                    throw err;
                }
            }
        }
    }
}
