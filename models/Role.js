const { Model } = require('objection');
const { customModel } = require('./mixins');


class Role extends customModel(Model) {
    static get tableName() {
        return 'roles';
    }

    static get relationMappings() {
        return {
            users: {
                relation: Model.ManyToManyRelation,
                modelClass: require('./User').User,
                join: {
                    from: 'roles.id',
                    through: {
                        from: 'users_roles.role_id',
                        to: 'users_roles.user_id'
                    },
                    to: 'users.id'
                }
            }
        }
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['name', 'common_name'],

            properties: {
                id: { type: 'integer' },
                name: { type: 'string', minLength: 1 },
                common_name: { type: 'string', minLength: 1 }
            }
        }
    }

    static async get(id) {
        const role = await Role.query()
            .findById(id);

        return role;
    }

    static async insert(data) {
        const role = await Role.query()
            .insertAndFetch(data);

        return role
    }

    static async update(id, data) {
        const role = await Role.query()
            .updateAndFetchById(id, data);

        return role;
    }

    static async delete(id) {
        const role = await Role.query()
            .deleteById(id);

        return role;
    }

    static async search(opts) {
        const query = Role.query()
            .limit(opts.limit)
            .offset((opts.page - 1) * opts.limit)

        if (opts.name) {
            query
                .where('name', 'like', `%${opts.name}%`)
                .orWhere('common_name', 'like', `%${opts.name}%`)
        }

        return await query;
    }

    static async getAll() {
        return await Role.query();
    }
}

module.exports = { Role };
