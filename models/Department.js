const { Model } = require('objection');
const { customModel } = require('./mixins');


class Department extends customModel(Model) {
    static get tableName() {
        return 'departments';
    }

    static get relationMappings() {
        return {
            users: {
                relation: Model.HasManyRelation,
                modelClass: require('./User').User,
                join: {
                    from: 'departments.id',
                    to: 'users.department_id'
                }
            }
        }
    }

    // This is used for validation only and doesn't
    // necessarily reflect the db schema
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['name'],

            properties: {
                id: { type: 'integer' },
                name: { type: 'string', minLength: 1 }
            }
        }
    }

    // Get a single department
    static async get(id, opts) {
        opts = opts || {};

        const query = Department.query();

        if (opts.userCount) {
            query.select (
                'departments.*',
                Department.relatedQuery('users').count().as('users_count')
            );
        }

        return await query.findById(id);
    }

    // insert a department
    static async insert(data) {
        const department = await Department.query()
            .insertAndFetch(data);

        return department
    }

    // update a department
    static async update(id, data) {
        const department = await Department.query()
            .updateAndFetchById(id, data);

        return department;
    }

    // delete a department
    static async delete(id) {
        const department = await Department.query()
            .deleteById(id);

        return department;
    }

    // fetch a list of departments
    static async search(opts) {
        opts = opts || {};

        const query = Department.query()
            .limit(opts.limit)
            .offset((opts.page - 1) * opts.limit)
            .orderBy(opts.sortBy, opts.sortOrder);

        if (opts.name) {
            query.where('name', 'like', `%${opts.name}%`);
        }

        if (opts.userCount) {
            query.select (
                'departments.*',
                Department.relatedQuery('users').count().as('users_count')
            );
        }

        return await query;
    }
}

module.exports = { Department };
