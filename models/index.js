module.exports = {
    ...require('./User'),
    ...require('./Role'),
    ...require('./Department')
};
