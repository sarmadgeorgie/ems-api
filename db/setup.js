const { Model } = require('objection');
const Knex = require('knex');

/**
 * Initialize connection to database and bind to ORM
 */
async function setupDB(config) {
    const knex = Knex(config);
    Model.knex(knex);
}

module.exports.setupDB = setupDB;
