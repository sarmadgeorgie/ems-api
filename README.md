# Employees Management System

## Requirements

- MySQL server
- NodeJS (tested on version 8.11)

## Setup

### Project Setup

Clone the project and install dependencies:

```bash
npm install
```

Copy the example environment file:

```bash
cp example.env .env
```

Modify the values in the .env file to match your environment, specifically the database config

### Database Setup

Create an empty database (names can be configured as you wish):

```sql
CREATE DATABASE ems;
```

Create a database user and give them access to the created database only, this is recommended over using the root user in the application:

```sql
CREATE USER 'ems'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON ems.* TO 'ems'@'localhost';
FLUSH PRIVILEGES;
```

This is the point at which you enter the values you used into the .env file.

### Migrating The Database Schema

Install Knex cli globally:

```bash
npm install -g knex
```

To run knex migrations:

```bash
knex migrate:latest
```

To seed the database:

```bash
knex seed:run
```

### Starting The Application

To start a server with live reload:

```bash
npm start
```
