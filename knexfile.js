require('dotenv').config();
const { knexConfig } = require('./config');

module.exports = {
    development: knexConfig,
    staging: knexConfig,
    production: knexConfig
};
